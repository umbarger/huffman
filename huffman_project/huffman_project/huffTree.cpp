//
//  huffTree.cpp
//  huffman_project
//
//  Created by Brian Umbarger on 8/8/13.
//  Copyright (c) 2013 Brian Umbarger. All rights reserved.
//

#include "huffTree.h"


huff_node* huff_tree::create(char a[], double x)
{
  huff_node* this_node;
  
  // allocate assign values to variables 
  this_node = new huff_node(x);
  
  // assign a to cha
  strcpy(&this_node -> cha, a);
  
  // assign x to frq
  this_node -> frq = x;
  
  // assign both left and right as NULL
  this_node -> lt = NULL;
  this_node -> rt = NULL;
  
  //return the address for this_node
  return (this_node);
}

void huff_tree::sort(std::vector<huff_node*> a, double n )
{
  
  // nested loop switches nodes if the frequency is higher
  for (int i = 0 ; i < n - 1; i++)
  {
    for (int j = i; j < n; j++)
    {
      if ( a[i]->frq > a[j] -> frq )
      {
        std::swap(a[i],a[j]);
      }
    }
  }
}

void huff_tree::shift_right( std::vector<huff_node*> a, int n )
{
  int i;
  for (i = 1 ; i < n - 1 ; i++)
  {
    a[i] = a[i+1];
  }
}

void huff_tree::make_code(huff_node* this_tree, int code[], int n)
{
  if (( this_tree -> lt == NULL) && (this_tree -> rt == NULL))
  {
    std::cout << this_tree -> cha << " ";
    
    for (int i = 0 ; i < n ; i++)
    {
      std::cout << code[i];
    }
    
    std::cout << std::endl;
  }
}

void huff_tree::delete_tree(huff_node* root)
{
  if(!(root == NULL))
  {
    delete_tree(root -> lt);
    delete_tree(root -> rt);
    delete root;
  }
}


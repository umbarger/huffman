//
//  huffTree.h
//  huffman_project
//
//  Created by Brian Umbarger on 8/8/13.
//  Copyright (c) 2013 Brian Umbarger. All rights reserved.
//

#ifndef __huffman_project__huffTree__
#define __huffman_project__huffTree__

#include <iostream>
#include <string>
#include <vector>



class huff_node
{
public:
  double frq;
  char cha;
  huff_node * lt;
  huff_node * rt;
  
  huff_node(double x, huff_node *l = NULL, huff_node *r = NULL)
  {
    frq = x;
    rt = r;
    lt = l;
  }
};

class huff_tree
{
public:
  huff_node *root;
  
  huff_tree()
  {
    root=NULL;
  }
  
  void sort(std::vector<huff_node*>, double);
  huff_node* create(char[], double);
  void shift_right(std::vector<huff_node*>, int);
  void make_code(huff_node*, int [], int);
  void delete_tree(huff_node *);
};


#endif /* defined(__huffman_project__huffTree__) */

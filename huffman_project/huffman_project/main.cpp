//
//  main.cpp
//  huffman_project
//
//  Created by Brian Umbarger on 8/5/13.
//  Copyright (c) 2013 Brian Umbarger. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include "huffTree.h"

using namespace std;

bool openFile(fstream &, char *);
void printVectors(vector<char>, vector<double>);

int main(int argc, const char * argv[])
{
  fstream data_file;
  
  huff_tree tree;
  int c[15];
  vector <char> char_array;
  vector <double> freq_array;
  vector <char> sorted_chars;
  vector <double> sorted_frq;
  vector <huff_node*> node_array;
  vector <string> binary_array;
  
  
  if (argc == 0)
  {
    cout << "Usage: program <filename>" << endl;
    return 0;
  }
  else
  {
    string file_name_string = argv[1];
    char file_name_char[file_name_string.length() + 1];
    strcpy(file_name_char, file_name_string.c_str());
    
    if (!openFile(data_file, file_name_char))
    {
      cout << "Error opening file!" << endl;
      cout << "Usage: program <filename>" << endl << "Please include the full path to file." << endl;
      return 0;
    }
  }
  
  string line;
  vector <string> tokens;
  
  while(getline(data_file, line))
  {
    istringstream iss(line);
    
    copy(istream_iterator<string>(iss),
         istream_iterator<string>(),
         back_inserter<vector<string>>(tokens));
  }
  
  
  for(int i = 0; i < tokens.size(); i++)
  {
    string a;
    a = tokens[i].data();
    char b[a.length()+1];
    strcpy(b,a.c_str());
    char_array.push_back(b[0]);
    
    i++;
    
    a = tokens[i].data();
    double c = atof(a.c_str());
    freq_array.push_back(c);
    
  }
  
  // Selection sort the arrays according to frequency.
  for(int i = 0 ; i < (freq_array.size() - 1) ; i++)
  {
    int min = i;
    for ( int j = (i + 1) ; j < (freq_array.size()) ; j++)
    {
        if ( freq_array[j] < freq_array[min])
        {
          min = j;
        }
      swap(freq_array[i],freq_array[min]);
      swap(char_array[i], char_array[min]);
    }
  }
  
  // Create nodes using all the values
  for (int i = 0; i < freq_array.size(); i++)
  {
    char a = char_array[i];
    char ary[] = { a, '\0'};
    double b = freq_array[i];
    huff_node &temp = tree.create(ary,b);
    node_array.push_back(temp);
  }
  
  for (int i = node_array.size() ; i > 1 ; i--)
  {
    tree.sort(node_array,node_array.size());
    string str;
    double combined_frq;
    char add_ary[] = {node_array[i] -> cha, node_array[i-1] -> cha};
    combined_frq = node_array[i] -> frq + node_array[i-1] -> frq;
    huff_node *parent = tree.create(add_ary, combined_frq);
    parent -> rt = node_array[i-1];
    parent -> lt = node_array[i];
    node_array.push_back(parent);
    tree.shift_right(node_array, i);
    tree.make_code(node_array[i], c, 0);
    tree.delete_tree(node_array[0]);
  }
  
  for(int i = 0; i < 15; i++)
  {
    cout << c[i];
  }
  
  
  
  for (int i = 0 ; i < node_array.size(); i++)
  {
    
    huff_node *this_node = node_array[i];
    cout << this_node -> cha << " " << this_node -> frq << endl;
  }
  
  
  return 0;
}

bool openFile(fstream &theFile, char *fileName)
{
  theFile.open(fileName, ios::in);
  if (theFile.fail())
    return false;
  else
    return true;
}

void printVectors(vector<char> char_array, vector<double> freq_array)
{
  for (int i = 0 ; i < char_array.size() ; i++)
  {
    cout << char_array[i] << " ";
  }
  
  cout << endl;
  
  for (int i = 0 ; i < freq_array.size() ; i++)
  {
    cout << freq_array[i] << " ";
  }
  
  cout << endl;
}
